class Car {
    brand = '';
    color = '';
    price = 0.0;

    constructor(brand, color) {
        this.brand = brand;
        this.color = color;
    }

    printDetails() {
        console.log("Brand: " + this.brand);
        console.log("Color: " + this.color);
        console.log("Price: " + this.price);
    }
}

let myCar = new Car("Ford", "red");
myCar.printDetails();
// console.log(myCar.brand);
// console.log(myCar.color);

console.log("----------");

let hisCar = new Car("Ferari", "yellow");
hisCar.printDetails();
// console.log(hisCar.brand);
// console.log(hisCar.color);

function manufactureBicycles() {
    let myBicycle = "Test";
    console.log(myCar);
    console.log(hisCar);
}

manufactureBicycles();
