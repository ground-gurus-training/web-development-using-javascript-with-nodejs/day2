/*
const demo = document.getElementById("demo");
demo.innerHTML = "Hello World";

const gurus = document.getElementsByClassName("groundgurus");

for (let guru of gurus) {
    guru.innerHTML = "Ground Gurus";
}

demo.setAttribute("class", "groundgurus");

demo.style.textAlign = "center";

document.querySelector("p#demo").setAttribute("style", "color: red");

const btn = document.querySelector("#btn");

let messages = "";

btn.onclick = function () {
   messages += "Hey, stop it!<br>";
   document.querySelector("#output").innerHTML = messages;
};
*/

const showResult = document.querySelector("#showResult");
showResult.onclick = function () {
    const height = document.querySelector("#height").value;
    document.querySelector("#heightOutput").innerHTML =
        "Your height is " + height + "cm";
};

let name = prompt("What is your name?");

if (!name) {
    name = "Guest";
}

document.querySelector("#output").innerHTML = "Hello " + name;
