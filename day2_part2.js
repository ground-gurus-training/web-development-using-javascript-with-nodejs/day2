document.querySelector("#search").onclick = function () {
    const monsterName = document.querySelector("#monsterName").value;

    fetch(`https://mhw-db.com/monsters?q={"name": "${monsterName}"}`)
    .then(response => response.json())
    .then(data => {
        // set the data to the fields
        const name = data[0].name;
        const description = data[0].description;
        document.querySelector("#name").innerHTML = name;
        document.querySelector("#description").innerHTML = description;

        // show the result
        document.querySelector("div.mdc-card")
            .setAttribute("style", "display: inline-block");
    });
};

